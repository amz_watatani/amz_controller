<?php
namespace Amz\Controller;

/**
 * コントローラーのベースクラス
 *
 * POSTパラメーター、セッションユーザー情報を取得
 * バリデーションの実行
 * サービスに必要なパラメーターを渡して実行(HTTPの関心事はサービスに持ち込まない)
 * HTTPレスポンスを返す
 *
 */
abstract class Controller
{

}
